# Distribute Aid Home Page

[![Netlify Status](https://api.netlify.com/api/v1/badges/b9d7a5c1-708f-4304-b739-96ff9e8a2a85/deploy-status)](https://app.netlify.com/sites/heuristic-gates-48838d/deploys)

This is the home page for [Distribute Aid](distributeaid.org).

## Up & Running

1. [Install Hugo](https://gohugo.io/getting-started/installing/)
2. Clone the project: `git clone git@gitlab.com:distribute-aid/landing.git`
3. Run the dev server (`hugo server`) and open [http://localhost:1313] in your browser.

## Deployment

The site is automatically deployed through [Netlify](https://www.netlify.com/).
